require 'json'
require 'rmagick'
require 'nokogiri'

class Converter
    def initialize(in_filename, out_filename, convert_for_kiwijs = false)
        @in_filename = in_filename
        @out_filename = out_filename
        @convert_for_kiwijs = convert_for_kiwijs
    end

    def convert!
        prepare_params
        load_map
        convert_tsx_tilesets
        extract_effective_tiles
        create_effective_tileset
        create_new_map
        write_output
    end

    private

    def prepare_params
        @base_out_name = File.basename(@out_filename, '.*')
        @base_out_dir = File.dirname(@out_filename)
        @base_dir = File.dirname(@in_filename)
    end

    def load_map
        @map = JSON.load(File.open(@in_filename))
    end

    def convert_tsx_tilesets
        @map['tilesets'].map! do |tileset|
            next tileset if tileset['image']

            # puts "Converting tileset #{tileset.inspect}"

            tileset_filename = File.join(@base_dir, tileset['source'])

            doc = Nokogiri::XML(File.open(tileset_filename))
            tileset_params = doc.at('tileset').to_h
            image_params = doc.at('image').to_h

            tile_count = (image_params['width'].to_i / tileset_params['tilewidth'].to_i) * (image_params['height'].to_i / tileset_params['tileheight'].to_i)

            {
                'name' => tileset_params['name'],
                'tilewidth' => tileset_params['tilewidth'],
                'tileheight' => tileset_params['tileheight'],
                'image' => image_params['source'],
                'imagewidth' => image_params['width'],
                'imageheight' => image_params['height'],
                'firstgid' => tileset['firstgid'],
                'margin' => tileset['margin'] || 0,
                'spacing' => tileset['spacing'] || 0,
                'properties' => {},
                'tilecount' => tile_count,
                'transparentcolor' => '#f4bdb0'
            }
        end
    end

    def extract_effective_tiles
        tiles_used = @map['layers'].map { |layer| layer['data'].uniq }.flatten.uniq.sort

        # puts "Tiles used: #{tiles_used.inspect}"

        @new_gid = {}
        cnt = 0

        @effective_tiles = @map['tilesets'].map do |tileset|
            tile_width, tile_height = tileset['tilewidth'].to_i, tileset['tileheight'].to_i

            cols = tileset['imagewidth'].to_i / tile_width
            rows = tileset['imageheight'].to_i / tile_height

            first_gid = tileset['firstgid'].to_i
            last_gid = first_gid + (cols * rows)

            src = File.join(@base_dir, tileset['image'])

            tile_ids = tiles_used.select { |tgid| tgid >= first_gid - 1 && tgid <= last_gid }

            # puts "Tiles in this tileset (#{first_gid} .. #{last_gid}): #{tile_ids.inspect}"

            tileset_img = Magick::Image.read(src)[0]

            tile_ids.map do |tgid|
                n = tgid - first_gid
                x = (n % cols).to_i * tile_width
                y = (n / cols).to_i * tile_height

                tile_pixels = tileset_img.dispatch(x, y, tile_width, tile_height, "RGBA").flatten

                img = Magick::Image.constitute(tile_width, tile_height, "RGBA", tile_pixels)
                img.alpha(Magick::ActivateAlphaChannel)

                @new_gid[tgid] = cnt
                cnt += 1

                img
            end
        end.flatten

        # puts "Effective tiles: #{@effective_tiles.inspect}"
    end

    def create_effective_tileset
        @new_tileset_size = Math.sqrt(@effective_tiles.size).to_i + 1

        @new_width = @new_tileset_size * @map['tilewidth'].to_i
        @new_tileset = Magick::Image.new(@new_width, @new_width) {
            self.background_color = 'none'
        }
        @new_tileset.alpha(Magick::ActivateAlphaChannel)
        @new_tileset.background_color = 'none'

        @tile_size = @map['tilewidth'].to_i

        # combine all the tiles into one image and replace original GIDs with the new ones
        @effective_tiles.each_with_index do |tile, index|
            if !@convert_for_kiwijs
                index += 1
            end

            y = (index / @new_tileset_size) * @tile_size
            x = (index % @new_tileset_size) * @tile_size

            @new_tileset.composite!(tile, x, y, Magick::OverCompositeOp)
        end
    end

    def create_new_map
        @new_tileset_filename = "#{@base_out_name}.png"

        @map['layers'].map! do |layer|
            layer['data'].map! do |tgid|
                gid = @new_gid[tgid] || 0

                # Kiwi.js counts from 1
                if @convert_for_kiwijs
                    gid == 0 ? 0 : gid + 1
                else
                    gid
                end
            end

            layer
        end

        @map['tilesets'] = [{
                'name' => 'merged tiles',
                'tilewidth' => @tile_size,
                'tileheight' => @tile_size,
                'image' => @new_tileset_filename,
                'imagewidth' => @new_width,
                'imageheight' => @new_width,
                'firstgid' => 1,
                'margin' => 0,
                'spacing' => 0,
                'properties' => {},
                'tilecount' => @effective_tiles.size,
                'transparentcolor' => '#ffffff'
            }]
    end

    def write_output
        @new_tileset.write(File.join(@base_out_dir, @new_tileset_filename))
        File.write(@out_filename, JSON.dump(@map))
    end
end

converter = Converter.new(ARGV[0], ARGV[1], true)
converter.convert!

# ruby converter.rb maps/map2.json maps/map2_converted.json
