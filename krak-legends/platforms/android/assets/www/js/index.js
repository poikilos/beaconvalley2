var tileSize = 32;

var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        window.state = new Kiwi.State('state');

        state.oldX = 0;
        state.oldY = 0;
        state.mapDragEvt = false;

        state.preload = function() {
            this.addJSON('tilemap', './res/maps/map3_converted.json');
            this.addSpriteSheet('tiles', './res/maps/map3_converted.png', 32, 32); // taken from image itself
            this.addSpriteSheet('characterSprite', './res/tilesets/knight.png', 32, 32); // taken from image
            this.addAudio('background', './res/sounds/krakow-background.mp3');
        }

        state.create = function() {
            this.tilemap = new Kiwi.GameObjects.Tilemap.TileMap(this, 'tilemap', this.textures.tiles);

            this.addChild(this.tilemap.layers[0]);
            this.addChild(this.tilemap.layers[1]);
            this.addChild(this.tilemap.layers[2]);
            this.addChild(this.tilemap.layers[3]);
            this.addChild(this.tilemap.layers[4]);
            this.addChild(this.tilemap.layers[5]);
            this.addChild(this.tilemap.layers[6]);

            this.character = new Kiwi.GameObjects.Sprite(this, this.textures.characterSprite, 384, 256);

            this.character.animation.add('idle-down', [0], 0.1, true, true);
            this.character.animation.add('idle-left', [12], 0.1, true, true);
            this.character.animation.add('idle-right', [24], 0.1, true, true);
            this.character.animation.add('idle-up', [36], 0.1, true, true);

            this.character.animation.add('move-down', [0, 1, 2], 0.1, true, true);
            this.character.animation.add('move-left', [12, 13, 14], 0.1, true, true);
            this.character.animation.add('move-right', [24, 25, 26], 0.1, true, true);
            this.character.animation.add('move-up', [36, 37, 38], 0.1, true, true);

            this.addChild(this.character);

            this.character.animation.play('idle-down');
            this.character.transform.x = 148 * tileSize;
            this.character.transform.y = 15 * tileSize;

            this.backgroundMusic = new Kiwi.Sound.Audio(this.game, 'background', 0.3, true);

            this.centerCameraToPlayer();

            // initialize UI elements
            var hudContainer = document.querySelector('#HUDContainer .kiwi-display');

            // popup
            hudContainer.innerHTML += '<div id="popup"></div>';

            // stars counter
            hudContainer.innerHTML += '<div id="stars"></div>';

            // logo
            hudContainer.innerHTML += '<div id="logo-container"><img src="./res/tilesets/logo.png"></div>';

            // run the demo!
            var actions = [
                function () {
                    state.backgroundMusic.play();
                },
                function () {
                    app.showPopup("You arrived to Kraków.<br/>Try looking for interesting places to get bonuses all around the city!");
                    return 6000;
                },
                function () {
                    app.showLogo();
                    return 5000;
                },
                function () {
                    app.hideLogo();
                    return 3000;
                }
            ];

            app.runDemo(actions, 0);
        };

        state.centerCameraToPlayer = function () {
            game.cameras.defaultCamera.transform.x = -(state.character.transform.x) + (10 * tileSize);
            game.cameras.defaultCamera.transform.y = -(state.character.transform.y) + (5 * tileSize);
        };

        // drag map with finger
        document.addEventListener("touchstart", function (e) {
            state.oldX = e.touches[0].clientX;
            state.oldY = e.touches[0].clientY;
            state.touchStartEvt = e;
            state.mapDragEvt = false;
        });

        document.addEventListener("touchmove", function (e) {
            var currX = e.touches[0].clientX || 0,
                currY = e.touches[0].clientY || 0,
                dx = currX - state.oldX,
                dy = currY - state.oldY,
                speed = 20,
                camX = game.cameras.defaultCamera.transform.x,
                camY = game.cameras.defaultCamera.transform.y,
                windowWidth = game.stage.width,
                windowHeight = game.stage.height
                mapWidth = state.tilemap.widthInPixels,
                mapHeight = state.tilemap.heightInPixels,
                tx = parseInt(dx / speed) * tileSize,
                ty = parseInt(dy / speed) * tileSize;

            if ((camX + tx < -1 * (mapWidth - windowWidth) && dx < 0) || (camX + tx > 0 && dx > 0))
                tx = 0;

            if ((camY + ty < -1 * (mapHeight - windowHeight) && dy < 0) || (camY + ty > 0 && dy > 0))
                ty = 0;

            game.cameras.defaultCamera.transform.x += tx;
            game.cameras.defaultCamera.transform.y += ty;

            state.oldX = currX;
            state.oldY = currY;

            if (tx != 0 || ty != 0) {
                state.mapDragEvt = true;
            }

            e.preventDefault();
        }, false);

        document.addEventListener('touchend', function (e) {
            var displayElt = document.querySelector('#HUDContainer .kiwi-display');
            var closeBtn = document.querySelector('#popup .button');

            if (e.target == closeBtn) {
                app.hidePopup();
                return;
            }

            // button handler
            if (e.target != displayElt) {
                // alert('Not a displayElt');
                return;
            }

            // if the touchend is just a map drag - drop off
            if (state.mapDragEvt == true) {
                return;
            }

            // move the hero to the position pointed
            var pointedX = -1 * game.cameras.defaultCamera.transform.x + state.touchStartEvt.touches[0].clientX,
                pointedY = -1 * game.cameras.defaultCamera.transform.y + state.touchStartEvt.touches[0].clientY;

            pointedX -= pointedX % tileSize;
            pointedY -= pointedY % tileSize;

            state.character.destX = pointedX;
            state.character.destY = pointedY;

            e.preventDefault();
        });

        setInterval(function () {
            if (state.character.destX || state.character.destY) {
                if ((Math.abs(state.character.transform.x - state.character.destX) < tileSize / 4) &&
                    (Math.abs(state.character.transform.y - state.character.destY) < tileSize / 4)) {
                    state.character.destX = null;
                    state.character.destY = null;
                    state.character.animation.play('idle-down');
                    console.log('arrived at: ', state.character.transform.x, state.character.transform.y);
                } else {
                    var dx = (state.character.destX - state.character.transform.x);
                    var dy = (state.character.destY - state.character.transform.y);

                    // direction only
                    dx = dx == 0 ? 0 : (dx / Math.abs(dx));
                    dy = dy == 0 ? 0 : (dy / Math.abs(dy));

                    if (dx < 0)
                        state.character.animation.play('move-left');
                    else if (dx > 0)
                        state.character.animation.play('move-right');
                    else if (dy < 0)
                        state.character.animation.play('move-up');
                    else if (dy > 0)
                        state.character.animation.play('move-down');

                    state.character.transform.x += (dx * tileSize);
                    state.character.transform.y += (dy * tileSize);

                    state.centerCameraToPlayer();
                }
            }
        }, 200);

        state.update = function() {
            Kiwi.State.prototype.update.call(this);
        };

        state.aimPlayerTo = function (x, y) {
            state.character.destX = x;
            state.character.destY = y;
        };

        window.game = new Kiwi.Game(null, 'New Tilemap Game', state);
        game.stage.resize(screen.width, screen.height);
    },
    hidePopup: function (msg) {
        var popup = document.querySelector('#popup');

        popup.classList.remove('visible');
    },
    showPopup: function (msg) {
        var popup = document.querySelector('#popup');

        popup.classList.add('visible');
        popup.innerHTML = msg + '<a class="button" href="#">Ok</a>';

        document.querySelector('#popup .button').addEventListener('click', function () {
            app.hidePopup();
        });
    },
    showLogo: function () {
        var logoContainer = document.querySelector('#logo-container');

        logoContainer.classList.add('visible');
    },
    hideLogo: function () {
        var logoContainer = document.querySelector('#logo-container');

        logoContainer.classList.remove('visible');
    },
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);

        console.log('Received Event: ' + id);
    },
    onPlaceFound: function (title, story) {
        app.showPopup("You have found " + title + "!<br>" + story);

        var placesVisited = parseInt(window.localStorage.getItem("placesCount") || 0);

        window.localStorage.setItem("placesCount", placesVisited + 1);

        var starsElt = document.querySelector('#stars');

        starsElt.innerHTML += '<div class="star"></div>';
    },
    runDemo: function (actions, actionIndex) {
        if (actionIndex >= actions.length)
            return;

        var delay = actions[actionIndex]();

        setTimeout(function () {
            delay = app.runDemo(actions, actionIndex + 1);
        }, delay);
    }
};