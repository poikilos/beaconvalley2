package com.eight_bit_heroes.krak_legends;

/**
 * Created by shybovycha on 22/11/15.
 */
public class Place {
    private String title;
    private String story;

    public Place(String title, String story) {
        this.title = title;
        this.story = story;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
