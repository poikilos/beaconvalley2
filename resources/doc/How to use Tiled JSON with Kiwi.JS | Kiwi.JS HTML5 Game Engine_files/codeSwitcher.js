//Get all of the switcher divs
var codes = document.getElementsByClassName('jsts-switcher');

//Loop through them all.
for(var i = 0; i < codes.length; i++) {
	
	//Get any ts/js elements inside of them.
	var ts = codes[i].getElementsByClassName('ts');
	var js = codes[i].getElementsByClassName('js');

	//Do they exist? If not ignore
	if(typeof ts[0] != "undefined" && typeof js[0] != "undefined") {

		//They do exist so continue.
		addSwitcher( codes[i], ts[0], js[0] );

	} 
}

function addSwitcher( mainDiv, jsDiv, tsDiv ) {
	//Shut down if any param not there.
	if(typeof mainDiv == "undefined" || typeof jsDiv == "undefined" || typeof tsDiv == "undefined") return false;


	//Hide the ts and show js
	jsDiv.className = "js jsts-switcher-hidden";
	tsDiv.className = "ts jsts-switcher-visible";

	//Add the buttons.
	var newContain = document.createElement('div');
	var newTsLabel = document.createElement('label');
	var newJsLabel = document.createElement('label');
	var checkBox = document.createElement('div');
	var newCheck = document.createElement('input');
	var text = document.createElement('span');

	text.innerHTML = 'View code as: ';
	newTsLabel.innerHTML = 'Typescript';
	newJsLabel.innerHTML = 'Javascript';

	newCheck.type = 'checkbox';
	newCheck.checked = false;
	checkBox.className = 'jsts-checkbox js';
	newJsLabel.className = 'current';
	newContain.className = 'jsts-switcher-buttons';
	mainDiv.className = mainDiv.className + ' supported';

	//Add then
	newContain.appendChild(text);
	newContain.appendChild(newTsLabel);
	newContain.appendChild(checkBox);
	checkBox.appendChild(newCheck);
	newContain.appendChild(newJsLabel);
	mainDiv.appendChild(newContain);

	//Events.
	newTsLabel.onclick = switchToTS;
	newJsLabel.onclick = switchToJS;
	checkBox.onclick = switchToggle;
}

function switchToJS() {
	var mainDiv = this.parentNode.parentNode;
	
	switchMainTo(mainDiv, 'TS');
}

function switchToTS() {
	var mainDiv = this.parentNode.parentNode;
	
	switchMainTo(mainDiv, 'JS');
}

function switchToggle() {
	var mainDiv = this.parentNode.parentNode;
	var state = getCurrentState(mainDiv);
	if(state == null) return;

	switchMainTo(mainDiv, state);

}

function switchMainTo(mainDiv, switchTo) {
	if(typeof mainDiv == "undefined" || typeof switchTo == "undefined") return;

	if(switchTo == 'JS') {
		mainDiv.getElementsByClassName('ts')[0].className = 'ts jsts-switcher-hidden'; //hide ts
		mainDiv.getElementsByClassName('js')[0].className = 'js jsts-switcher-visible'; //show js
		mainDiv.getElementsByClassName('jsts-checkbox')[0].className = 'jsts-checkbox ts';
		mainDiv.getElementsByTagName('label')[0].className = 'current';					
		mainDiv.getElementsByTagName('label')[1].className = '';
		mainDiv.getElementsByTagName('input')[0].checked = true; //uncheck tickbox cause we wanting js to be shown

	} else if(switchTo == 'TS') {
		mainDiv.getElementsByClassName('ts')[0].className = 'ts jsts-switcher-visible';//show ts
		mainDiv.getElementsByClassName('js')[0].className = 'js jsts-switcher-hidden';//hide js
		mainDiv.getElementsByClassName('jsts-checkbox')[0].className = 'jsts-checkbox js';
		mainDiv.getElementsByTagName('label')[0].className = '';
		mainDiv.getElementsByTagName('label')[1].className = 'current';
		mainDiv.getElementsByTagName('input')[0].checked = false; //check tickbox cause ts is being shown

	}
}

function getCurrentState(mainDiv) {
	var check = mainDiv.getElementsByTagName('input');

	if(typeof check[0] != "undefined") {
		
		if(check[0].checked === true) { //if the checkbox is checked then

			return 'TS';			//is is displayed
		} else {
			
			return 'JS';			//js is displayed.
		}
	}
	return null;
}
